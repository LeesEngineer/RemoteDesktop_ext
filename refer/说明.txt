kanaka-noVNC-v0.6.1-47
	是官方网站下载的发布版本。在这个基础上做了一个小扩展(支持web文件管理器kodexplorer3.23链接)


setup-x86.exe
	cygwin编译环境
	
		
tightvnc
	是支持文件传输的vnc，也能跟我们的程序无缝支持
	
web-ftp	
	是各种web文件管理器，以及php的xammp
	
jre-7u79-windows-i586.exe
	我们打开libVNC的网页界面，总是会先发现说插件不支持，是因为需要java的支持
	所以我们需要先安装java。不能安装java8
	虽然我们可以直接用HTML5的方式浏览，但是总是无法支持文件传输
	
libvncserver-LibVNCServer-0.9.10.zip
	我们的libvncserver源码